﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Boundary
{
	public float xMin, xMax, zMin, zMax;
}
public class PlayerMovement : MonoBehaviour
{
	public float speed;
	public float tilt;
	public Boundary boundary; 

	public GameObject shot;
	public Transform shotSpawn;
	public float fireRate;
	public Vector3[] playerpositions;
	public GameObject playerExplosion;

	public GUIText reloadingText;

	private float nextFire;
	public int maxAmmo = 15;
	private int currentAmmo;
	public float reloadTime = 1f;
	private bool isReloading = false;

	public SpriteRenderer tankSprite;

	public Sprite[] tankSprites;



	public GameController gameController;
	public GameObject[] myLives;
	int lives = 3;
	public void LoseLife() {

		lives--;

		print ("Life:" + lives);

		myLives[lives].SetActive(false);

		if (lives <= 0) {
			Instantiate (playerExplosion, transform.position, transform.rotation);
			gameController.GameOver();
			Destroy(gameObject);
		}
	}

	void Start ()
	{ 
		{
			int randomNumber = Random.Range (0, playerpositions.Length);
			transform.position = playerpositions [randomNumber];
		}
			
		currentAmmo = maxAmmo;
		reloadingText.text = "";
	}
	
	void Update ()
	{
		if (Input.GetButton("Fire1")&&Time.time > nextFire && currentAmmo > 0) //have 3 bullets. if you shoot three times, then you are out of bullets, and cannot shoot anymore.
		{
			currentAmmo--; //each fire reduces bullet amount by one

			nextFire = Time.time + fireRate;
			Instantiate(shot, shotSpawn.position, shotSpawn.rotation);
			reloadingText.text = "";
			GetComponent<AudioSource>().Play();
		}

		if (isReloading)  
			return;
		
		if (currentAmmo <= 0) {
				StartCoroutine(Reload());
			reloadingText.text = "Reloading...";
			return;
		}
			
	}

		IEnumerator Reload ()
		{
			isReloading = true;
			reloadingText.text = "Reloading...";
			yield return new WaitForSeconds(reloadTime);

			currentAmmo = maxAmmo;
			isReloading =false;
		}
			
	void FixedUpdate ()
	{
		float moveHorizontal = Input.GetAxis ("Horizontal");
		float moveVertical = Input.GetAxis ("Vertical");

		if (Mathf.Abs (moveHorizontal) > Mathf.Abs (moveVertical)) {
			if (moveHorizontal > 0) {
				tankSprite.sprite = tankSprites [3];
				print ("right");
			} else {
				print ("left");
				tankSprite.sprite = tankSprites [2];
			}

		} else {
			if (moveVertical > 0) {
				print ("up");
				tankSprite.sprite = tankSprites [0];
			} else {
				print ("down");
				tankSprite.sprite = tankSprites [1];
			} 
		}

		Vector3 movement = new Vector3 (moveHorizontal, 0.0f, moveVertical);
		GetComponent<Rigidbody> ().velocity = movement * speed;

		GetComponent<Rigidbody> ().position = new Vector3 
		(
				Mathf.Clamp(GetComponent<Rigidbody>().position.x, boundary.xMin, boundary.xMax),
			0.0f,
			Mathf.Clamp(GetComponent<Rigidbody>().position.z, boundary.zMin, boundary.zMax)
			);
		GetComponent<Rigidbody>().rotation = Quaternion.Euler (0.0f, 0.0f, GetComponent<Rigidbody>().velocity.x * -tilt);
	}
		
}
